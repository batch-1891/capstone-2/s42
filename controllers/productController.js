const Product = require('../models/Product');


module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return user
		}

	})
}