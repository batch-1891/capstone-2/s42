const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return user
		}

	})
}

module.exports.registerAdmin = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10), 
		isAdmin: true
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return user
		}

	})
}

module.exports.userAuthentication = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}

	})
}

