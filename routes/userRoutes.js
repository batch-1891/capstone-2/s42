const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');



router.post("/registerUser", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/userAuthentication", (req, res) => {
	userController.userAuthentication(req.body).then(resultFromController => res.send(resultFromController))
});




module.exports = router;