const express = require('express');
const router = express.Router();
const userController = require('../controllers/productController');

router.post('/createProduct', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});


module.exports = router;