const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Price is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', productSchema)